-- Exercise 4


-- 1 Evaluation Relation

inductive FCPLang where
  | nat (n : Nat) : FCPLang
  | bool (b : Bool) : FCPLang
  | ite (a : FCPLang) (b : FCPLang) (c : FCPLang) : FCPLang
  deriving Repr

namespace FCPLang


-- 1.1 Single-Step

def eval1 (t : FCPLang) : Option FCPLang :=
  match t with
  | ite (bool true) pos _ => some pos
  | ite (bool false) _ neg => some neg
  | ite cond pos neg => match (eval1 cond) with
    | some (bool b) => some (ite (bool b) pos neg)
    | some (ite a b c) => some (ite (ite a b c) pos neg)
    | _ => none
  | t' => some t'

inductive Eval₁ : FCPLang -> FCPLang -> Prop
  | iteTrue (pos neg : FCPLang) : Eval₁ (ite (bool true) pos neg) pos
  | iteFalse (pos neg : FCPLang) : Eval₁ (ite (bool false) pos neg) neg
  | iteCongruence (t₁ t₁' : FCPLang) (ev : Eval₁ t₁ t₁') : Eval₁ (ite t₁ pos neg) (ite t₁' pos neg)

infixl:60 " ->₁ " => Eval₁


-- 1.2 Multi-Step
inductive Evaln : FCPLang -> FCPLang -> Prop
  | eval1Base (t₁ t₂ : FCPLang) (ev : Eval₁ t₁ t₁') : Evaln t₁ t₁'
  | eval1Steps (t₁ t₂ t₃ : FCPLang) 
               (ev₁ : Eval₁ t₁ t₂)
               (ev₂ : Eval₁ t₂ t₃) : Evaln t₁ t₃



-- 2 Type Checking

-- 2.1 Type of FCPLang-Terms
inductive FCPType where
  | tBool : FCPType
  | tNat : FCPType

open FCPType

-- 2.2 Type Checking Function
--def typecheck (t : FCPLang) (type : FCPType) : Bool :=
def typecheck : FCPLang -> FCPType -> Bool
  | (bool _), tBool => true
  | (nat _), tNat => true
  | ite cond pos neg, type => 
      match (typecheck cond .tBool), (typecheck pos type), (typecheck neg type) with
        | true, true, true => true
        | _,_,_ => false 
  | _,_ => false

-- 2.3 Type Checking Relation
inductive Welltyped : FCPLang -> FCPType -> Prop
  | bTypeBool (b : Bool) : Welltyped (bool b) tBool
  | nTypeNat (n : Nat) : Welltyped (nat n) tNat
  | iteTyped (expectedType : FCPType) (cond : FCPLang)
             (ev₁ : Welltyped cond tBool)
             (ev₂ : Welltyped pos expectedType)
             (ev₃ : Welltyped neg expectedType) : Welltyped (ite cond pos neg) expectedType
      
infixl:60 " :' " => Welltyped

-- 2.4 Equivalence of Type Checking
theorem typecheck_eq_Welltyped : ∀ (t τ), (t :' τ) -> (typecheck t τ = true) := by
  intro t τ ev 
  induction ev
  case bTypeBool b => 
    rw [typecheck]
  case nTypeNat n =>
    rw [typecheck]
  case iteTyped pos neg expectedType cond e f g h i j => 
    rw [typecheck]
    simp
    rw [h, i, j]
  done



-- 3 Progress & Preservation

-- 3.1 Value Proposition
inductive IsValue : FCPLang -> Prop
  | boolVal (b : Bool) : IsValue (bool b)
  | natVal (n : Nat) : IsValue (nat n)

-- 3.2 Canonical Forms 
theorem bool_IsValue : ∀ t, (Welltyped t tBool) -> (IsValue t) ->
  (∃ b, t = bool b) := by 
  intro t tTyped tIsVal
  cases tTyped
  case bTypeBool b => 
    exists b
  case iteTyped cond pos neg Hneg Hcond Hpos  =>
    cases tIsVal

theorem nat_IsValue : ∀ t, (Welltyped t tNat) -> (IsValue t) ->
  (∃ n, t = nat n) := by
  intro t tTyped tIsVal
  cases tIsVal
  case boolVal b =>
    cases tTyped
  case natVal n =>
    cases tTyped
    case nTypeNat =>
      exists n

theorem t_IsValue : ∀ t τ, (Welltyped t τ) -> (IsValue t) ->
  (∃ b, t = bool b) ∨ (∃ n, t = nat n) := by 
  intro τ type tTyped tIsValue
  cases tTyped
  case bTypeBool b =>
    apply Or.inl
    exists b
  case nTypeNat n =>
    apply Or.inr
    exists n
  case iteTyped =>
    cases tIsValue

-- 3.3 Progress
theorem fcpLangProgress : ∀ t τ, (Welltyped t τ) ->
  (IsValue t) ∨ (∃ t', t ->₁ t') := by
  intro t τ tTyped
  induction tTyped
  case bTypeBool b =>
    apply Or.inl
    case h => apply (.boolVal b)
  case nTypeNat n =>
    apply Or.inl
    case h => apply (.natVal n)
  case iteTyped pos neg _ cond e _ _ h _ _ =>
    apply Or.inr
    case h => 
      cases e
      case bTypeBool b' => 
        cases b'
        case false => 
          exists neg
          apply (.iteFalse pos neg)
        case true =>
          exists pos
          apply (.iteTrue pos neg)
      case iteTyped x y z x' y' z' => 
        cases h
        case inl m => cases m
        case inr m => 
          cases m
          case intro t'' n' => 
            exists (ite t'' pos neg)
            apply (.iteCongruence (ite z x y) t'' n')
    
      
-- 3.4 Preservation
theorem fcpLangPreservation : ∀ t t' τ, (Welltyped t τ) -> (t ->₁ t') -> 
  (Welltyped t' τ) := by
  intro t t' τ H1 H2
  revert τ
  induction H2
  case iteTrue pos neg => 
    intro τ H3
    cases H3
    case iteTyped J K L => apply K
  case iteFalse pos neg => 
    intro τ H3
    cases H3
    case iteTyped J K L => apply L
  case iteCongruence pos neg cond cond' _ condTypedImpl =>
    intro τ H2
    cases H2
    case iteTyped X Y Z =>
      have h := condTypedImpl _ X
      apply .iteTyped τ cond' h Y Z



-- 4 Bonus Exercises

-- 4.1 Extending 2.4
theorem Welltyped_eq_typecheck : ∀ (t τ), (typecheck t τ = true) -> (t :' τ) := by
  intro t τ H
  revert τ
  induction t
  case nat n => 
    intro τ H1
    cases τ
    case tBool => cases H1
    case tNat => apply .nTypeNat n
  case bool b =>
    intro τ H1
    cases τ
    case tBool => apply .bTypeBool b
    case tNat => cases H1
  case ite cond pos neg H2 H3 H4 =>
    intro τ H1
    unfold typecheck at H1
    cases K : (typecheck cond tBool)
    case false => 
      rewrite [K] at H1
      cases H1
    case true => 
      specialize H2 _ K
      cases L : (typecheck pos τ)
      case false =>
        rewrite [K,L] at H1
        cases H1
      case true => 
        specialize H3 _ L
        cases M : (typecheck neg τ)
        case false =>
          rewrite [K,L,M] at H1
          cases H1
        case true =>
          specialize H4 _ M
          apply (.iteTyped τ cond H2 H3 H4)

theorem Welltyped_equiv_typecheck : ∀ (t τ), (t :' τ) <-> 
  (typecheck t τ = true) := by
  intro t τ
  apply Iff.intro
  case mp => apply typecheck_eq_Welltyped
  case mpr => apply Welltyped_eq_typecheck

-- 4.2 Big-Step Produces Value
def eval (t : FCPLang) : Option FCPLang :=
  match t with
  | ite cond iftrue iffalse => match (eval cond) with
    | (bool true) => eval iftrue
    | (bool false) => eval iffalse
    -- not necessary to match `ite`, `(eval cond)` will find a normal form
    -- that helps us choosing a branch if it exists
    | _ => none 
  | t' => some t'

theorem bigstep_produces_value : ∀ t t', (eval t = some t') -> (IsValue t') :=
  by
  intro t t' H
  revert t'
  induction t
  case nat n => 
    intro t' H
    cases H
    case refl => constructor
  case bool b =>
    intro t' H
    cases H
    case refl => constructor
  case ite cond pos neg _ b c => 
    intro t' H
    unfold eval at H
    cases K : (eval cond)
    case none =>
      rewrite [K] at H
      cases H
    case some b =>
      cases b
      case nat n => 
        rewrite [K] at H
        contradiction
      case bool b =>
        rewrite [K] at H
        cases b
        case false => 
          simp at H
          specialize c _ H
          apply c
        case true =>
          simp at H
          specialize b _ H
          apply b
      case ite cond' pos' neg' =>
        rw [K] at H
        simp at H

          
    