set_option autoImplicit false

-- Exercise 3 

inductive FCPLang where
  | nat (n : Nat) : FCPLang
  | bool (b : Bool) : FCPLang
  | ite (a : FCPLang) (b : FCPLang) (c : FCPLang) : FCPLang
  deriving Repr

namespace FCPLang


def eval (t : FCPLang) : Option FCPLang :=
  match t with
  | ite cond iftrue iffalse => match (eval cond) with
    | (bool true) => eval iftrue
    | (bool false) => eval iffalse
    -- not necessary to match `ite`, `(eval cond)` will find a normal form
    -- that helps us choosing a branch if it exists
    | _ => none 
  | t' => some t'



-- 1.2 Proofs

theorem eval_ite_nat : ∀ (n t e), 
  FCPLang.eval (.ite (.nat n) t e) = failure := by 
  intro n t e
  rfl
  done

theorem eval_ite_true : ∀ (t e), 
  FCPLang.eval (.ite (.bool true) t e) = FCPLang.eval t := by
  intro t e
  rfl
  done

theorem eval_ite_false : ∀ (t e), 
  FCPLang.eval (.ite (.bool false) t e) = FCPLang.eval e := by
  intro t e
  rfl
  done


-- 2.1 Small Step Semantics

def eval1 (t : FCPLang) : Option FCPLang :=
  match t with
  | ite (bool true) pos _ => some pos
  | ite (bool false) _ neg => some neg
  | ite cond pos neg => match (eval1 cond) with
    | some (bool b) => some (ite (bool b) pos neg)
    | some (ite a b c) => some (ite (ite a b c) pos neg)
    | _ => none
  | t' => some t'

def evaln (t : FCPLang) : Option FCPLang := 
  match t with
  | ite a b c => match eval1 (ite a b c) with
    | some t' => evaln t'
    | none => none
  | t' => some t'
termination_by evaln t => t
decreasing_by sorry


-- 2.2 Proofs

theorem evaln_ite_true : ∀ t e, FCPLang.evaln (.ite (.bool true) t e) = FCPLang.evaln t := by sorry
theorem evaln_ite_false : ∀ t e, FCPLang.evaln (.ite (.bool false) t e) = FCPLang.evaln e := by sorry

theorem eval1_nat : ∀ n, FCPLang.eval1 (.nat n) = FCPLang.nat n := by
  intro n
  rfl
  done
theorem eval1_bool : ∀ b, FCPLang.eval1 (.bool b) = FCPLang.bool b := by
  intro n
  rfl
  done
theorem eval1_ite_true : ∀ t e, FCPLang.eval1 (.ite (.bool true) t e) = t := by
  intro t e
  rfl
  done
theorem eval1_ite_false : ∀ t e, FCPLang.eval1 (.ite (.bool false) t e) = e := by
  intro t e
  rfl
  done

-- 3.1 Big-Step = Small-Step

theorem evaln_ite_some {cond cond' :  FCPLang} (pos neg : FCPLang) :
  (evaln cond = some cond') →
  FCPLang.evaln (.ite cond pos neg) = FCPLang.evaln (.ite cond' pos neg) := by sorry


theorem evaln_cond_ne_bool_evaln_ite_eq_none {t} (pos neg) :
  (∀ b, ¬ evaln t = bool b) → evaln (ite t pos neg) = none := by sorry

 

theorem big_eq_small : ∀ t, eval t = evaln t := by
  intro t
  induction t
  case nat n => 
    unfold eval
    unfold evaln
    rfl
  case bool b => 
    unfold eval
    unfold evaln
    rfl
  case ite cond pos neg Hcond Hpos Hneg =>
    cases cond
    case nat n =>
      rw [eval, Hcond, evaln]
      unfold evaln
      unfold eval1
      simp
      unfold eval1
      simp
    case bool b => 
      cases b
      case false =>
        rw [evaln_ite_false, eval, Hcond, Hneg]
        unfold evaln
        simp
      case true => 
        rw [evaln_ite_true, eval, Hcond, Hpos]
        unfold evaln
        simp
    case ite c p n =>
      rewrite [evaln, eval, Hcond, Hpos, Hneg, <-evaln]
      split
      case h_1 M N =>
        rewrite [<-Hpos]
        rw [evaln_ite_some pos neg N, evaln_ite_true, Hpos]
      case h_2 M N => 
        rewrite [<-Hneg]
        rw [evaln_ite_some pos neg N, evaln_ite_false, Hneg]
      case h_3 M N O =>
        rw [evaln_cond_ne_bool_evaln_ite_eq_none]
        case a =>
          intro b H
          rw [H] at N
          rw [H] at O
          cases b
          case false =>
            apply O   
            rfl
          case true =>
            apply N
            rfl



-- 4.1 Term Equivalence

def Equiv (t1 t2 : FCPLang) : Prop := evaln t1 = evaln t2
--notation t1:60 " ≡ " t2:60 => Equiv t1 t2
infixl:60 " ≡ " => Equiv


-- 4.2 Properties of Term Equivalence

theorem Equiv_refl : ∀ t1 : FCPLang, Equiv t1 t1 := by
  intro t1
  rfl

theorem Equiv_symm : ∀ t1 t2 : FCPLang, Equiv t1 t2 -> Equiv t2 t1 := by
  intro t1 t2 H
  unfold Equiv
  rw [H]

theorem Equiv_trans : ∀ t1 t2 t3 : FCPLang, Equiv t1 t2 -> Equiv t2 t3
  -> Equiv t1 t3 := by
  intro t1 t2 t3 H1 H2
  unfold Equiv
  rw [H1, H2]