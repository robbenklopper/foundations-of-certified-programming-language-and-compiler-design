inductive Term
  | bool (b : Bool)
  | nat  (n : Nat)
  | ite  (cond pos neg : Term)
  deriving Repr

namespace Term

def eval : Term → Option Term
  | bool b => bool b
  | nat n  => nat n
  | ite cond pos neg => 
    match eval cond with
    | bool true  => eval pos
    | bool false => eval neg
    | _          => none

def eval1 : Term → Option Term
  | bool b                  => bool b
  | nat n                   => nat n
  | ite (bool true) pos _   => pos
  | ite (bool false) _ neg  => neg
  | ite (nat _) _ _         => none
  | ite (ite c p n) pos neg => 
    match eval1 (ite c p n) with
    | some cond => ite cond pos neg
    | none      => none

def evaln (t : Term) : Option Term :=
  match h : eval1 t with
  | none                    => none
  | some (bool b)           => bool b
  | some (nat n)            => nat n
  | some (ite cond pos neg) => evaln (ite cond pos neg)
termination_by evaln t => t
decreasing_by sorry


#eval Term.eval (.bool true)
#eval Term.eval (.ite (.bool true) (.nat 1) (.bool false))
#eval Term.eval (.ite (.nat 1) (.bool true) (.bool false))
#eval Term.eval (.ite (.ite (.bool true) (.bool true) (.bool false)) (.bool true) (.bool false))

#eval Term.eval1 (.bool true)
#eval Term.eval1 (.ite (.bool true) (.nat 1) (.bool false))
#eval Term.eval1 (.ite (.nat 1) (.bool true) (.bool false))
#eval Term.eval1 (.ite (.ite (.bool true) (.bool true) (.bool false)) (.bool true) (.bool false))

#eval Term.evaln (.bool true)
#eval Term.evaln (.ite (.bool true) (.nat 1) (.bool false))
#eval Term.evaln (.ite (.nat 1) (.bool true) (.bool false))
#eval Term.evaln (.ite (.ite (.bool true) (.bool true) (.bool false)) (.bool true) (.bool false))


-- 2.2 Proofs

theorem evaln_ite_true : ∀ t e, Term.evaln (.ite (.bool true) t e) = Term.evaln t := by sorry
theorem evaln_ite_false : ∀ t e, Term.evaln (.ite (.bool false) t e) = Term.evaln e := by sorry

theorem eval1_nat : ∀ n, Term.eval1 (.nat n) = Term.nat n := by
  intro n
  rfl
  done
theorem eval1_bool : ∀ b, Term.eval1 (.bool b) = Term.bool b := by
  intro n
  rfl
  done
theorem eval1_ite_true : ∀ t e, Term.eval1 (.ite (.bool true) t e) = t := by
  intro t e
  rfl
  done
theorem eval1_ite_false : ∀ t e, Term.eval1 (.ite (.bool false) t e) = e := by
  intro t e
  rfl
  done

-- 3.1 Big-Step = Small-Step
theorem evaln_ite_some {cond cond' :  Term} (pos neg : Term) :
  (evaln cond = some cond') →
  Term.evaln (.ite cond pos neg) = Term.evaln (.ite cond' pos neg) := by sorry


theorem evaln_cond_ne_bool_evaln_ite_eq_none {t} (pos neg) :
  (∀ b, ¬ evaln t = bool b) → evaln (ite t pos neg) = none := by sorry

theorem big_eq_small : ∀ t, eval t = evaln t := by
  intro t
  induction t
  case bool b =>
    rw [eval, evaln]
    rfl
  case nat n =>
    rw [eval, evaln]
    rfl
  case ite cond pos neg Hcond Hpos Hneg =>
    cases cond
    case nat n =>
      rw [eval, eval, evaln, eval1]
    case bool b => 
      cases b 
      case false => 
        rw [evaln_ite_false]
        rw [eval, Hcond, Hneg, evaln, eval1]
      case true =>
        rw [evaln_ite_true]
        rw [eval, Hcond, Hpos, evaln, eval1]
    case ite c p n =>
      rewrite [evaln, eval, Hcond, Hpos, Hneg, <-evaln]
      split
      case h_1 M N =>
        rewrite [<-Hpos]
        rw [evaln_ite_some pos neg N, evaln_ite_true, Hpos]
      case h_2 M N => 
        rewrite [<-Hneg]
        rw [evaln_ite_some pos neg N, evaln_ite_false, Hneg]
      case h_3 M N O =>
        rw [evaln_cond_ne_bool_evaln_ite_eq_none]
        case a =>
          intro b H
          rw [H] at N
          rw [H] at O
          cases b
          case false =>
            apply O   
            rfl
          case true =>
            apply N
            rfl
