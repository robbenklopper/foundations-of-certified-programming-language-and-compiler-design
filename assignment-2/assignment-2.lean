-- Exercise 2 Inductive Data Types


-- 1 Resources



-- 2 Inductive Data Types

--  2.1 Natural Numbers
--   2.1.1
inductive MyNat where
  | zero : MyNat
  | succ (n : MyNat) : MyNat
  deriving Repr

def MyNat.one := succ zero
def MyNat.two := succ (succ zero)
def MyNat.three := succ (succ (succ zero)) 

--   2.1.2
def MyNat.add (x y : MyNat) : MyNat :=
  match x with
  | zero => y
  | (succ n) => succ (add n y)

infixl:65 " + " => MyNat.add -- https://leanprover.github.io/lean4/doc/notation.html
-- infixl:50 " + " => add -- stolen from Coq, software foundations
#eval MyNat.one
#eval MyNat.add MyNat.one MyNat.two

#check MyNat.zero + (MyNat.succ MyNat.zero)


--  2.2 Lists - Polymorphic Types
--  2.3 Polymorphic Lists
inductive PolyList (α : Type) where
  | nil
  | cons (hd : α) (tl : PolyList α)
  deriving Repr

infixr:60 " :: " => PolyList.cons

--   2.3.1
def length {α : Type} (l : PolyList α) : MyNat :=
  match l with
  | PolyList.nil => .zero
  | PolyList.cons _ tl => .succ (length tl)

#eval length (PolyList.cons MyNat.one PolyList.nil)

--   2.3.2
def append {α : Type} (l1 l2 : PolyList α) : PolyList α :=
  match l1 with
  | PolyList.nil => l2
  | PolyList.cons hd tl => PolyList.cons hd (append tl l2)

infixr:60 " ++ " => append


#eval append (PolyList.cons MyNat.zero PolyList.nil) 
             (PolyList.cons MyNat.zero PolyList.nil)

--   2.3.3
def PolyList.reverse {α : Type} (l : PolyList α) : PolyList α :=
  match l with
  | PolyList.nil => PolyList.nil
  | PolyList.cons hd tl => 
      append (reverse tl) (PolyList.cons hd PolyList.nil)

#eval PolyList.reverse (PolyList.cons MyNat.one (PolyList.cons MyNat.two PolyList.nil))



-- 3 Tactics

-- copy/pasted from the respective exercise sheet
theorem nat_eq_trans : ∀ x y z : Nat, (x = y ∧ y = z) → x = z := by
  intro x y z h
  apply Eq.trans
  · have hx := h.left
    exact hx
  · have hy := h.right
    exact hy
    done

--  3.1 First Proofs
theorem nat_eq_refl : ∀ x : MyNat, x = x := by
  apply Eq.refl
  done

theorem alpha_eq_refl : ∀ (α : Type) (x : α), x = x := by
  apply Eq.refl
  done

theorem nat_eq_symm : ∀ x y : MyNat, x = y → y = x := by
  apply Eq.symm
  done

theorem alpha_eq_symm : ∀ (α : Type) (x y : α), x = y -> y = x := by
  apply Eq.symm
  done



-- 4 Inductive Proofs

--  4.1 Induction on natural numbers
-- copy/pasted from the respective exercise sheet
theorem MyNat.assoc : ∀ x y z : MyNat, x + (y + z) = (x + y) + z := by
  intro x y z
  induction x
  case zero => rfl
  case succ n h => rw [add, h, add, ←add]
  done

theorem MyNat.add_zero : ∀ x : MyNat, x + zero = x := by
  intro x
  induction x
  case zero => rfl
  case succ n h => rw [add, h]
  done

theorem MyNat.add_succ : ∀ x y : MyNat, x + (succ y) = succ (x + y) := by
  intro x y
  induction x
  case zero => rfl
  case succ n h => rw [add, h, add]
  done

theorem MyNat.comm : ∀ x y : MyNat, x + y = y + x := by
  intro x y
  induction x
  case zero => rw [add, add_zero]
  case succ n h => rw [add, h, ←add_succ]
  done

-- 4.2 Induction on Lists

theorem PolyList.rev_neutral {α : Type} : 
  ∀ (a : α), a :: nil = reverse (a::nil):= by
  intro a
  rfl
  done

theorem PolyList.append_nil_r {α : Type} : ∀ l : PolyList α,
  append l nil = l := by
  intro l
  induction l
  case nil => rw [append]
  case cons hd tl h => rw [append, h]
  done

theorem PolyList.append_assoc {α : Type} : ∀ (l1 l2 l3 : PolyList α),
  l1 ++ (l2 ++ l3) = (l1 ++ l2) ++ l3 := by
  intro l1 l2 l3
  induction l1
  case nil => 
    rw [append, append]
  case cons hd tl h => 
    rw [ append, append, append, h]
  done

theorem PolyList.rev_swap_dist {α : Type} : ∀ (l1 l2 : PolyList α), 
  reverse (l1 ++ l2) = (reverse l2) ++ (reverse l1) := by
  intro l1 l2
  induction l1
  case nil => 
    rw [reverse, append, append_nil_r]
  case cons hd tl h => 
    rw [append, reverse, reverse, h, append_assoc]
  done

theorem PolyList.selfinverse {α : Type} : 
  ∀ list : PolyList α, reverse (reverse list) = list := by 
  intro l
  induction l
  case nil => rfl
  case cons hd tl h => 
    rw [reverse] 
    rw [rev_swap_dist, h, <-rev_neutral, append, append]
  done