Copy `file.holbert` to `./holbert/index.holbert` and restart holbert (see
`./holbert/README.md). This assumes that holbert was installed to `./holbert`.

Tar containing a holbert distribution was obtained from:
<https://www.barkhauseninstitut.org/fileadmin/FCPLCD/WS2022-23/assignment-1.tar.gz>

A hard reload might be required in firefox: Ctrl-Shift-R.

For the sake of completeness:

`cd ./holbert`

`python3 -m http.server 8000`
