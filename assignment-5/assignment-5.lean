-- Exercise 5


-- 1 Propositional Logic

-- 1.1 Connectives
inductive True' : Prop 
  | T

inductive False' : Prop

inductive And' : Prop -> Prop -> Prop
  | and' (t1 t2 : Prop) (ev1 : t1) (ev2 : t2) : And' t1 t2

inductive Or' : Prop -> Prop -> Prop
  | orl' (t1 t2 : Prop) (ev : t1) : Or' t1 t2
  | orr' (t1 t2 : Prop) (ev : t2) : Or' t1 t2

inductive Equiv' : Prop -> Prop -> Prop
  | equiv' (t1 t2 : Prop) (ev1 : t1 -> t2) (ev2 : t2 -> t1) 
  : Equiv' t1 t2

inductive Not' : Prop -> Prop
  | not' (t1 : Prop) (ev : t1 -> False') : Not' t1

-- 1.2 Proofs
theorem and_impl_or : ∀ a b, And' a b -> Or' a b := 
  λ a b ev => 
    match ev with 
    | .and' _ _ a' _ => (.orl' a b a')

theorem or_swap : ∀ a b, Or' a b -> Or' b a := 
  λ a b ev =>
    match ev with
    | .orl' _ _ a' => .orr' b a a'
    | .orr' _ _ b' => .orl' b a b'

theorem impl2 : forall (a b c : Prop), (a -> b -> c) -> ((And' a b) -> c) :=
  λ _ _ _ ev1 ev2 =>
    match ev2 with
    | .and' _ _ a' b' => ev1 a' b'


-- 2 First-Order Logic

-- 2.1 Natural Arithmetic
inductive Nat' :=
  | zero' : Nat'
  | succ' (n : Nat') : Nat'
  deriving Repr

def add' (x : Nat') (y : Nat') : Nat' :=
  match x with
  | .zero' => y
  | .succ' x' => .succ' (add' x' y)

def mult' (x : Nat') (y : Nat') : Nat' :=
  match x with
  | .zero' => .zero'
  | .succ' x' => add' y (mult' x' y)

-- 2.2 Equality

inductive eq' : Nat' -> Nat' -> Prop :=
  | eq' (x : Nat') : eq' x x

theorem symmetry' : forall x y, eq' x y -> eq' y x :=
  λ x _ H => 
    match H with 
    | .eq' _ => .eq' x 

theorem transitivity' : forall x y z, eq' x y -> eq' y z -> eq' x z := 
  λ _ _ _ M N =>
    match M with
    | .eq' _ => N

-- 2.3 Definition Equality 
theorem add0 : forall x, eq' (add' .zero' x) x := 
  λ x =>
    .eq' (add' .zero' x)

theorem addS : forall x y, eq' (add' (.succ' x) y) (.succ' (add' x y)) := 
  λ _ _ =>
    .eq' (add' _ _)

-- 2.4 Congruence
theorem cong_s : forall x y, eq' x y -> eq' (.succ' x) (.succ' y) := 
  λ x _ H =>
    match H with
    | .eq' _ => .eq' (.succ' x)

theorem cong_add : forall x1 x2 y1 y2, eq' x1 x2 -> eq' y1 y2 -> 
  eq' (add' x1 y1) (add' x2 y2) :=
  λ x1 _ y1 _ H1 H2 =>
    match H1 with 
    | .eq' _ => match H2 with
                | .eq' _ => .eq' (add' x1 y1)

-- 2.5 Holbert Revisited
theorem right_identity_plus : forall x, eq' (add' x .zero') x := 
  λ x =>
    match x with
    | .zero' => .eq' (add' .zero' .zero')
    | .succ' x' => (cong_s _ _ (right_identity_plus x'))

theorem left_identity_mult : forall x, eq' (mult' (.succ' .zero') x) x := 
  λ x =>
    match x with
    | .zero' => .eq' _
    | .succ' x' => cong_s _ _ (right_identity_plus x')

-- 3 Bonus Exercise
inductive G₁ : Nat' -> Prop :=
  | g1_base : G₁ (.succ' (.succ' .zero'))
  | g1_step (ev : G₁ n) : G₁ (.succ' n)

inductive L₄ : Nat' -> Prop :=
  | lt4_base : L₄ (.succ' (.succ' (.succ' .zero')))
  | lt4_step (ev : L₄ (.succ' n)) : L₄ n

inductive R₁₄ : Nat' -> Prop :=
  | range14 (ev1 : L₄ n) (ev2 : G₁ n) : R₁₄ n

theorem r14_3 : R₁₄ (.succ' (.succ' (.succ' .zero'))) := 
  .range14 .lt4_base (.g1_step .g1_base)

theorem not_g1_impl_not_r14_0 : (Not' (G₁ .zero')) -> (Not' (R₁₄ .zero')) :=
  λ H =>
    match H with
    | .not' _ H => 
      .not' _ (λ ev => match ev with 
                       | M => match M with
                              | .range14 _ G => H G)